// JavaScript Document
$(function(){
			$('ul.menu').slicknav({
				label: 'MENU',
				duration: 1000,
				prependTo:'.mobil-menu'
			});
		});
		
		function startSlide(){
			$('#slides').superslides({
				inherit_width_from: '#slider',
				inherit_height_from: '#slider',
				play: 3500,
				animation: 'fade',
				pagination: false,
		  	});	
		}
		
		$(document).ready(function(e) {
			if($('#slider').hasClass('slider-mini')){
				$('#slider').css('height', 185);
			}else{
				$('#slider').css('height', $(window).height());
			}
			startSlide();
			headerScroll();
			size();
			loadMapFooter()
        });
		
		$(window).scroll(function(e) {
            if($(window).width()>1079){
				headerScroll();
			}
        });
		
		function size(){

		}
		
		function headerScroll(){
			var scrollOnTop=$(window).scrollTop();
			var maxScroll=90;
			if(scrollOnTop<maxScroll){
				$('header').css('height', 185-scrollOnTop);
				$('.img-logo').attr('src', '/theme/ujenika/img/logo.png');
				$('.img-logo').attr('height', 150-scrollOnTop);
				$('header > .wrapper > .right > .contact').css('top', (0-(scrollOnTop*1.5)));
				$('header > .wrapper > .right > ul.menu').css('margin-top', (10-(scrollOnTop)));
				$('header').css('background-color', 'rgba(13, 134, 54, '+(0.7+(scrollOnTop/320))+')');
				$('nav.down').css('opacity', 1-(scrollOnTop/50));
				$('nav.down').css('display', 'block');
			}else{
				$('header').css('height', 185-maxScroll);
				$('.img-logo').attr('src', '/theme/ujenika/img/logo_small.png');
				$('.img-logo').attr('height', 150-maxScroll);
				$('header > .wrapper > .right > .contact').css('top', (0-(maxScroll*1.5)));
				$('header > .wrapper > .right > ul.menu').css('margin-top', (10-(maxScroll)));
				$('header').css('background-color', 'rgba(13, 134, 54, 1)');
				$('nav.down').css('opacity', 0);
				$('nav.down').css('display', 'none');
			}
		}
		
		$('.down').click(function(a){
			$(window.opera ? 'html' : 'html, body').animate({
					scrollTop: $(window).height()-60
			}, 2000); 	
		});
		
		  function loadMapFooter() {
			var mapOptions = {
			  center: { lat: 49.201906, lng: 16.622103},
			  zoom: 15,
			  disableDefaultUI: true
			};
			var map = new google.maps.Map(document.getElementById('map-footer'),
				mapOptions);
				
			var JenikLatLng = new google.maps.LatLng(49.202051, 16.607105);	
			 			 
			  var marker = new google.maps.Marker({
				position: JenikLatLng,
				map: map,
				title:"U Jeníka",
				icon: '/theme/ujenika/img/mapicon.png'
			  });
		  
		  }
		  
		  $(".contact-menu").click(function(e) {
				$('html, body').animate({
					scrollTop: $("#map-footer").offset().top-90
				}, 2000);
		  });